Name:           perl-Net-Server
Version:        2.014
Release:        3
Summary:        Extensible, general Perl server engine
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Net-Server
Source0:        https://cpan.metacpan.org/modules/by-module/Net/Net-Server-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  coreutils findutils make perl-generators perl-interpreter
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76 perl(strict) perl(warnings)
BuildRequires:  perl(base) perl(Carp) perl(Errno) perl(Exporter) perl(Fcntl) perl(File::Temp)
BuildRequires:  perl(IO::Handle) perl(IO::Multiplex) >= 1.05 perl(IO::Select) perl(IO::Socket)
BuildRequires:  perl(IO::Socket::IP) perl(IO::Socket::SSL) >= 1.31 perl(IO::Socket::UNIX)
BuildRequires:  perl(Net::SSLeay) perl(POSIX) perl(re) perl(Scalar::Util)
BuildRequires:  perl(Socket) perl(Socket6) perl(Time::HiRes) perl(vars) perl(constant) perl(lib)
BuildRequires:  perl(English) perl(File::Spec::Functions) perl(FindBin) perl(threads) perl(Test::More)

Requires:       perl(IO::Multiplex) >= 1.05  perl(IO::Socket::IP)

%description
This package designed to be the back-end layer of the internet protocol servers.
And it is an extensible, class-oriented module written by perl.

%package help
Summary:       Provides help docs for perl-Net-Server
Requires:      perl-Net-Server = %{version}-%{release}

%description help
This package provides man-pages and help docs for perl-Net-Server

%prep
%autosetup -n Net-Server-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT

%check
make test

%files
%license LICENSE
%doc Changes examples
%{perl_vendorlib}/*
%{_bindir}/net-server

%files help
%doc README
%{_mandir}/man3/*
%{_mandir}/man1/net-server.1*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 2.014-3
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Jul 19 2024 yaoxin <yao_xin001@hoperun.com> - 2.014-2
- License compliance rectification

* Fri Sep 8 2023 liyanan <thistleslyn@163.com> - 2.014-1
- update to 2.014

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 2.010-1
- Upgrade to version 2.010

* Thu Mar 12 2020 daiqianwen <daiqianwen@huawei.com> - 2.009-6
- delete useless buildrequire.

* Fri Nov 22 2019 sunguoshuai <sunguoshuai@huawei.com> - 2.009-5
- Package init.
